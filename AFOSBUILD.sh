rm -rf /opt/ANDRAX/chisel

if [ $(uname -m | grep 'x86_64') ]; then
   make linux
else
   make linux-static
fi

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make Linux... PASS!"
else
  # houston we have a problem
  exit 1
fi

make linux-arm

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make Linux ARM... PASS!"
else
  # houston we have a problem
  exit 1
fi

make windows

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make windows, ughhh... PASS!"
else
  # houston we have a problem
  exit 1
fi

make darwin

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make Darwin... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir /opt/ANDRAX/chisel

cd build

find . -executable -type f -exec cp -Rf {} /opt/ANDRAX/chisel \;

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy executable files... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf ../andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
